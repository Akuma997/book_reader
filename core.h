/**
  Portable book reader, core code.

  by Miloslav Ciz, 2019, released under CC0 1.0
*/

#include <stdint.h>

#define BRD_NEW_LINE (127 - 32)
#define BRD_PAGE_SEPARATOR 255
#define BRD_SPECIAL_CHAR_FROM (128 - 32)

typedef void (*BRD_PixelFunction)(uint16_t x, uint16_t y);

void BRD_init();
void BRD_nextPage();
void BRD_previousPage();
void BRD_offsetPage(int16_t offset);
void BRD_decodePage(uint16_t pageNumber);
void BRD_renderPage(BRD_PixelFunction pixelFunction, int8_t capitalize);
void BRD_renderPagePosition(
  BRD_PixelFunction pixelFunction, uint16_t x, uint16_t y);

uint16_t BRD_currentPage;

uint8_t *BRD_pageArray;

#include "data.h"
#include "font.h"

#include BRD_PLATFORM_HEADER

#define BRD_PAGE_ARRAY_SIZE (BRD_CHARS_X * BRD_CHARS_Y + 2)

uint8_t _BRD_pageArray[BRD_PAGE_ARRAY_SIZE];

char BRD_decodeChar(uint8_t d)
{
  return d != BRD_NEW_LINE ? (d + 32) : '\n';
}

void BRD_decodePage(uint16_t pageNumber)
{
  for (uint16_t i = 0; i < BRD_PAGE_ARRAY_SIZE; ++i)
    _BRD_pageArray[i] = 0;

  uint32_t pos = 0;
  uint16_t pageNo = 0;

  while (pageNo < pageNumber && pos < BRD_COMPRESSED_DATA_SIZE)
  {
    if (BRD_GET_PROGRAM_MEMORY_BYTE(BRD_compressedData,pos) == BRD_PAGE_SEPARATOR)
      pageNo++;

    pos++;
  }

  if (pageNo != pageNumber)
    return;

  BRD_currentPage = pageNumber;
 
  uint16_t pageLen = 0;

  while (1) // copy the raw compressed data
  {
    if (pos >= BRD_COMPRESSED_DATA_SIZE || BRD_GET_PROGRAM_MEMORY_BYTE(BRD_compressedData,pos) == BRD_PAGE_SEPARATOR)
      break;

    _BRD_pageArray[pageLen] = BRD_GET_PROGRAM_MEMORY_BYTE(BRD_compressedData,pos);

    pos++;
    pageLen++;
  }

  while (1) // keep expanding symbols
  {
    pos = 0;
  
    int8_t expanded = 0;

    while (pos < pageLen) // check every byte for potential expansion
    {
      if (_BRD_pageArray[pos] >= BRD_SPECIAL_CHAR_FROM) // symbol to expand?
      {
        // expand
 
        uint16_t pairIndex = (_BRD_pageArray[pos] - BRD_SPECIAL_CHAR_FROM) * 2;

        _BRD_pageArray[pos] = BRD_GET_PROGRAM_MEMORY_BYTE(BRD_compressionDictionary,pairIndex);

        // shift

        for (uint16_t i = BRD_PAGE_ARRAY_SIZE - 1; i > pos; --i)
          _BRD_pageArray[i] = _BRD_pageArray[i - 1]; 

        _BRD_pageArray[pos + 1] = BRD_GET_PROGRAM_MEMORY_BYTE(BRD_compressionDictionary,pairIndex + 1);

        pageLen++;

        expanded = 1;
      }

      pos++;
    }

    if (!expanded)
      break;
  }

  for (uint16_t i = 0; i < pageLen; ++i)
    _BRD_pageArray[i] = BRD_decodeChar(_BRD_pageArray[i]);

  _BRD_pageArray[BRD_PAGE_ARRAY_SIZE - 1] = 0;
}

void BRD_init()
{
  BRD_pageArray = _BRD_pageArray;

  BRD_decodePage(0);
}

void BRD_nextPage()
{
  if (BRD_currentPage < BRD_PAGE_COUNT - 1)
    BRD_decodePage(BRD_currentPage + 1);
}

void BRD_previousPage()
{
  if (BRD_currentPage > 0)
    BRD_decodePage(BRD_currentPage - 1);
}

void BRD_offsetPage(int16_t offset)
{
  if (offset > 0)
  {
    BRD_currentPage += offset;

    if (BRD_currentPage >= BRD_PAGE_COUNT)
      BRD_currentPage = BRD_PAGE_COUNT - 1;

    BRD_decodePage(BRD_currentPage);
  }
  else
  {
    if (-1 * offset <= BRD_currentPage)
      BRD_decodePage(BRD_currentPage + offset);
    else
      BRD_decodePage(0);
  }
}

void BRD_renderText(BRD_PixelFunction pixelFunction, const uint8_t *text,
  int8_t capitalize, uint16_t posX, uint16_t posY)
{
  uint16_t pos = 0;

  uint16_t currentY = posY;
  uint16_t x = posX;

  while (text[pos] != 0)
  {
    uint8_t c = text[pos];

    if (c == '\n')
    {
      currentY += BRD_FONT_HEIGHT + 1;
      x = 0;
    }
    else
    {
      c -= 32;

      if (capitalize && c >= 65 && c <= 90)
        c -= 32;

      uint16_t character = BRD_GET_PROGRAM_MEMORY_WORD(BRD_font,c);

      for (uint8_t i = 0; i < BRD_FONT_HEIGHT; ++i)
      {
        uint16_t y = currentY;

        for (uint8_t j = 0; j < BRD_FONT_WIDTH; ++j)
        {
          if (character & 0x8000)
            pixelFunction(x,y);

          y++;

          character = character << 1;
        }

        x++;
      }

      x++;
    }

    pos++;
  }
}

void BRD_renderPage(BRD_PixelFunction pixelFunction, int8_t capitalize)
{
  BRD_renderText(pixelFunction,BRD_pageArray,capitalize,0,0);
}

void BRD_renderPagePosition(
  BRD_PixelFunction pixelFunction, uint16_t x, uint16_t y)
{
  uint8_t buffer[10];

  buffer[9] = 0;

  uint8_t pos = 8;

  uint16_t num = BRD_PAGE_COUNT;

  while (num > 0)
  {
    buffer[pos] = '0' + (num % 10);
    num /= 10;
    pos--;
  }

  buffer[pos] = '/';  
  pos--;

  num = BRD_currentPage + 1;

  while (num > 0)
  {
    buffer[pos] = '0' + (num % 10);
    num /= 10;
    pos--;
  }

  BRD_renderText(pixelFunction,buffer + pos + 1,0,x,y);
}
