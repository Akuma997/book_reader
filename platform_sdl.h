#include <stdio.h>
#include <SDL2/SDL.h>
#include <unistd.h>

#define MARGIN 4

#define SCREEN_WIDTH ((BRD_FONT_WIDTH + 1) * BRD_CHARS_X + MARGIN * 2)
#define SCREEN_HEIGHT ((BRD_FONT_HEIGHT + 1) * (BRD_CHARS_Y + 2) + MARGIN * 2)

#define SCALE_UP 3

#define SCREEN_SIZE (SCREEN_WIDTH * SCREEN_HEIGHT * SCALE_UP * SCALE_UP)

uint16_t screen[SCREEN_SIZE];

void setPixel(uint16_t x, uint16_t y)
{
  x += MARGIN;
  y += MARGIN;

  uint32_t i = (y * SCREEN_WIDTH * SCALE_UP + x) * SCALE_UP;

  for (uint32_t y = 0; y < SCALE_UP; ++y)
  {
    for (uint32_t x = 0; x < SCALE_UP; ++x)
      screen[i + x] = 0xffff;

    i += SCREEN_WIDTH * SCALE_UP;
  }
}

int main(int argc, char *argv[])
{
  printf("starting\n");

  SDL_Window *window =
    SDL_CreateWindow("book", SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH * SCALE_UP, SCREEN_HEIGHT * SCALE_UP,
    SDL_WINDOW_SHOWN); 

  SDL_Renderer *renderer = SDL_CreateRenderer(window,-1,0);

  SDL_Texture *texture =
    SDL_CreateTexture(renderer,SDL_PIXELFORMAT_RGB565,SDL_TEXTUREACCESS_STATIC,
    SCREEN_WIDTH * SCALE_UP,SCREEN_HEIGHT * SCALE_UP);

  SDL_Surface *screenSurface = SDL_GetWindowSurface(window);

  BRD_init();

  int running = 1;

  int capitalize = 1;

  while (running)
  {
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
      switch (event.type)
      {
        case SDL_KEYDOWN:
          switch (event.key.keysym.scancode)
          {
            case SDL_SCANCODE_ESCAPE:
            case SDL_SCANCODE_Q: running = 0; break;

            case SDL_SCANCODE_C: capitalize = !capitalize; break;

            case SDL_SCANCODE_DOWN:
            case SDL_SCANCODE_RIGHT: BRD_nextPage(); break;

            case SDL_SCANCODE_UP:
            case SDL_SCANCODE_LEFT: BRD_previousPage(); break;

            default: break;
          }
          break;

        case SDL_QUIT:
          running = 0;
          break;

        default:
          break;
      }
    }

    for (uint32_t i = 0; i < SCREEN_SIZE; ++i)
      screen[i] = 0;

    BRD_renderPage(setPixel,capitalize);
    BRD_renderPagePosition(setPixel,BRD_FONT_WIDTH + 1,(BRD_FONT_HEIGHT + 1) * (BRD_CHARS_Y + 1));

    SDL_UpdateTexture(texture,NULL,screen,SCREEN_WIDTH * SCALE_UP * sizeof(uint16_t));

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer,texture,NULL,NULL);
    SDL_RenderPresent(renderer);

    usleep(10 * 1000);
  }    

  printf("ending\n");

  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer); 
  SDL_DestroyWindow(window); 

  return 0;
}
