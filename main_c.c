#define BRD_PROGRAM_MEMORY static const

#define BRD_GET_PROGRAM_MEMORY_BYTE(addr,offset) (addr[offset])
#define BRD_GET_PROGRAM_MEMORY_WORD(addr,offset) (addr[offset])

#include "core.h"
