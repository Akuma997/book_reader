#include <Arduboy2.h>

Arduboy2 arduboy;

#define REPEAT_PERIOD 20

#define EEPROM_START 160
#define EEPROM_VALID 36

uint8_t repeatCounter, bookmarkCounter;

void setup()
{
  arduboy.begin();
  arduboy.clear();

  repeatCounter = REPEAT_PERIOD;
  bookmarkCounter = 0;

  BRD_init();

  if (EEPROM.read(EEPROM_START) != EEPROM_VALID)
  {
    EEPROM.update(EEPROM_START,EEPROM_VALID);
    EEPROM.update(EEPROM_START + 1,0);
  }
  else
  {
    BRD_decodePage(EEPROM.read(EEPROM_START + 1));
  }
}

uint8_t buttonRegisers(int button)
{
  if (arduboy.justPressed(button))
  {
    repeatCounter = REPEAT_PERIOD;
    return 1;
  }
  
  return arduboy.pressed(button) && repeatCounter == 0;
}

void loop()
{
  if (!(arduboy.nextFrame()))
    return;

  arduboy.clear();

  arduboy.setCursor(1,1);

  arduboy.print((char *) BRD_pageArray);

  arduboy.pollButtons();

  repeatCounter -= 1;

  if (buttonRegisers(RIGHT_BUTTON))
    BRD_nextPage();
  else if (buttonRegisers(LEFT_BUTTON))
    BRD_previousPage();
  else if (buttonRegisers(DOWN_BUTTON))
    BRD_offsetPage(10);
  else if (buttonRegisers(UP_BUTTON))
    BRD_offsetPage(-10);

  if (arduboy.justPressed(A_BUTTON) && bookmarkCounter == 0)
  {
    EEPROM.write(EEPROM_START + 1,BRD_currentPage);
    bookmarkCounter = 50;
  }

  arduboy.setCursor(1,55);

  if (bookmarkCounter > 0)
  {
    arduboy.print("bookmark saved");
    bookmarkCounter--;
  }
  else
  {
    arduboy.print(BRD_currentPage + 1);
    arduboy.print(" / ");
    arduboy.print(BRD_PAGE_COUNT);
  }

  if (repeatCounter == 0)
    repeatCounter = REPEAT_PERIOD;

  arduboy.display();
}
