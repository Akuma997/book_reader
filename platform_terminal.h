/**
  Book reader frontend implementation for terminal.

  by Miloslav Ciz, 2019, released under CC0 1.0
*/

#include <stdio.h>

int main()
{
  BRD_init();

  while (1)
  {
    for (uint8_t i = 0; i < 50; ++i)
      printf("\n");

    printf("%s\n",BRD_pageArray);

    printf("\n-----------\npage %d/%d; command (n,p,q): ",BRD_currentPage + 1,BRD_PAGE_COUNT);
    
    char c = getchar();

    if (c == 'q')
      break;

    switch (c)
    {
      case 'n': BRD_nextPage(); break;
      case 'p': BRD_previousPage(); break;
      default: break;
    }
  }

  return 0;
}
