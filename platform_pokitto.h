/*
  Pokitto front end for book reader.

  Use screen mode 15! (high res)

  by Miloslav Ciz, released under CC0 1.0 
*/

#include "Pokitto.h"

Pokitto::Core pokitto;

#define MARGIN 4

void setPixel(uint16_t x, uint16_t y)
{
  pokitto.display.drawPixel(MARGIN + x,MARGIN + y,1);
}

int buttonRegisters(int button)
{
  int time = pokitto.buttons.timeHeld(button);

  return (time == 1) || ((time > 10) & (time % 4 == 0));
}

int main()
{
  pokitto.begin(); 

  pokitto.setFrameRate(32);
  pokitto.display.setFont(font5x7);
  pokitto.display.fontSize = 1;
  pokitto.display.setInvisibleColor(-1);

  pokitto.display.bgcolor = 8;
  pokitto.display.color = 1;

  BRD_init();

  while (pokitto.isRunning())
  {
    if (pokitto.update())
    {
      uint8_t x = MARGIN;
      uint8_t y = MARGIN;
      
      uint16_t pos = 0;

      char c = BRD_pageArray[pos];

      while (c != 0)
      {
        pokitto.display.print(x,y,c);

        if (c != '\n')
          x += pokitto.display.font[0] + 1;
        else
        {
          x = MARGIN;
          y += pokitto.display.font[1] + 4;
        }

        pos++;

        c = BRD_pageArray[pos];
      }

      pokitto.display.setCursor(MARGIN,176 - pokitto.display.font[1] - MARGIN);

      pokitto.display.print(BRD_currentPage + 1);
      pokitto.display.print(" / ");
      pokitto.display.print(BRD_PAGE_COUNT);

      if (buttonRegisters(BTN_A) || buttonRegisters(BTN_RIGHT))
        BRD_nextPage();
      else if (buttonRegisters(BTN_B) || buttonRegisters(BTN_LEFT))
        BRD_previousPage();
      else if (buttonRegisters(BTN_DOWN))
        BRD_offsetPage(10);
      else if (buttonRegisters(BTN_UP))
        BRD_offsetPage(-10);
    }
  }

  return 0;
}
